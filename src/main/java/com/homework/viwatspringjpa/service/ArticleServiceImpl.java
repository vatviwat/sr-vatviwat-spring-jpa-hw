package com.homework.viwatspringjpa.service;

import com.homework.viwatspringjpa.model.Article;
import com.homework.viwatspringjpa.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService{

    private ArticleRepository articleRepository;

    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public List<Article> getAllArticle() {
        return articleRepository.getAll();
    }

    @Override
    public Article getOneArticle(int id) {
        return articleRepository.getOne(id);
    }

    @Override
    public List<Article> getArticleByCategoryTitle(String categoryTitle) {
        return articleRepository.getArticleByCategoryTitle(categoryTitle);
    }

    @Override
    public Article updateArticle(int id,Article article) {
        return articleRepository.updateArticle(id,article);
    }

    @Override
    public Article deleteArticle(int id) {
        return articleRepository.deleteArticle(id);
    }

    @Override
    public Article saveArticle(Article article) {
        return articleRepository.saveArticle(article);
    }

}
