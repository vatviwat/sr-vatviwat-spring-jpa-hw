package com.homework.viwatspringjpa.service;

import com.homework.viwatspringjpa.model.Category;

import java.util.List;

public interface CategoryService {

    //TODO: get all category
    List<Category> getAllCategory();

    //TODO: get one category
    Category getOneCategory(int id);

    //TODO: update category
    Category updateCategory(int id,Category category);

    //TODO: delete category
    Category deleteCategory(int id);

    //TODO: save category
    Category saveCategory(Category category);
}
