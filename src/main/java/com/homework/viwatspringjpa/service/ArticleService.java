package com.homework.viwatspringjpa.service;

import com.homework.viwatspringjpa.model.Article;

import java.util.List;

public interface ArticleService {

    //TODO: get all article
    List<Article> getAllArticle();

    //TODO: get one article by id
    Article getOneArticle(int id);

    //TODO: get articles by category title
    List<Article> getArticleByCategoryTitle(String categoryTitle);

    //TODO: update article by id
    Article updateArticle(int id,Article article);

    //TODO: delete article by id
    Article deleteArticle(int id);

    //TODO: save article
    Article saveArticle(Article article);

}
