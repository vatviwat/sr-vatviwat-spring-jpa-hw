package com.homework.viwatspringjpa.service;

import com.homework.viwatspringjpa.model.Category;
import com.homework.viwatspringjpa.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService{

    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategory() {
        return categoryRepository.getCategory();
    }

    @Override
    public Category getOneCategory(int id) {
        return categoryRepository.getOneCategory(id);
    }

    @Override
    public Category updateCategory(int id,Category category) {
        return categoryRepository.updateCategory(id,category);
    }

    @Override
    public Category deleteCategory(int id) {
        return categoryRepository.deleteCategory(id);
    }

    @Override
    public Category saveCategory(Category category) {
        return categoryRepository.saveCategory(category);
    }
}
