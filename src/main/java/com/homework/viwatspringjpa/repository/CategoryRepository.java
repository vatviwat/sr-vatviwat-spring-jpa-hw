package com.homework.viwatspringjpa.repository;

import com.homework.viwatspringjpa.model.Article;
import com.homework.viwatspringjpa.model.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class CategoryRepository {
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @PersistenceContext
    private EntityManager entityManager;

    //TODO: get all category
    @Transactional
    public List<Category> getCategory(){
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Category> criteriaQuery = criteriaBuilder.createQuery(Category.class);
        Root<Category> categoryRoot = criteriaQuery.from(Category.class);
        criteriaQuery.select(categoryRoot);

        TypedQuery<Category> categoryTypedQuery = entityManager.createQuery(criteriaQuery);
        return categoryTypedQuery.getResultList();
    }

    //TODO: save category
    @Transactional
    public Category saveCategory(Category category){
        entityManager.persist(category);
        return category;
    }

    //TODO: get one category
    @Transactional
    public Category getOneCategory(int id){
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Category> criteriaQuery = criteriaBuilder.createQuery(Category.class);
        Root<Category> categoryRoot = criteriaQuery.from(Category.class);
        criteriaQuery.where(criteriaBuilder.equal(categoryRoot.get("id"),id));

        TypedQuery<Category> categoryTypedQuery = entityManager.createQuery(criteriaQuery);
        Category result = null;
        try{
            result = categoryTypedQuery.getSingleResult();
        }catch (NullPointerException|NoResultException exception){
            System.out.println(exception);
        }
        return result;
    }

    //TODO: update category
    @Transactional
    public Category updateCategory(int id,Category category) {
        Query query = entityManager.createQuery("UPDATE Category c SET c.title = :title WHERE c.id =:id")
                .setParameter("id",id)
                .setParameter("title",category.getTitle());
        query.executeUpdate();
        entityManager.clear();
        entityManager.find(Category.class,id);
        return entityManager.getReference(Category.class,id);
    }


    //TODO: delete category
    @Transactional
    public Category deleteCategory(int id) {
        Query query = entityManager.createQuery("Delete from Category c WHERE c.id = :id")
                .setParameter("id",id);
        Category category = new Category();
        query.executeUpdate();
        return category;
    }

}
