package com.homework.viwatspringjpa.repository;

import com.homework.viwatspringjpa.model.Article;
import com.homework.viwatspringjpa.model.Category;
import org.hibernate.PersistentObjectException;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class ArticleRepository {

    @PersistenceContext
    private EntityManager entityManager;

    //TODO: get all article
    public List<Article> getAll(){
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Article> criteriaQuery = criteriaBuilder.createQuery(Article.class);
        Root<Article> articleRoot = criteriaQuery.from(Article.class);
        criteriaQuery.select(articleRoot);

        TypedQuery<Article> articleTypedQuery = entityManager.createQuery(criteriaQuery);
        List<Article> articleList =  articleTypedQuery.getResultList();
        return articleList;
    }

    //TODO: get one article
    @Transactional
    public Article getOne(int id){
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Article> criteriaQuery = criteriaBuilder.createQuery(Article.class);
        Root<Article> articleRoot = criteriaQuery.from(Article.class);
        criteriaQuery.where(criteriaBuilder.equal(articleRoot.get("id"),id));

        TypedQuery<Article> articleTypedQuery = entityManager.createQuery(criteriaQuery);
        Article result = null;
        try{
            result = articleTypedQuery.getSingleResult();
        }catch (NullPointerException|NoResultException exception){
            System.out.println(exception);
        }
        return result;
    }

    //TODO: save article
    @Transactional
    public Article saveArticle(Article articleRequest) {
        try {
            entityManager.persist(articleRequest);
        }catch (PersistentObjectException e){

        }
        entityManager.clear();
        return entityManager.find(Article.class,articleRequest.getId());
    }

    //TODO: update article
    @Transactional
    public Article updateArticle(int id, Article article) {
        Query query = entityManager.createQuery("UPDATE Article a SET a.author = :author, a.title = :title," +
                "a.description = :description, a.category.id = :category_id WHERE a.id =:id")
                .setParameter("id",id)
                .setParameter("author",article.getAuthor())
                .setParameter("title",article.getTitle())
                .setParameter("description",article.getDescription())
                .setParameter("category_id",article.getCategory().getId());
        query.executeUpdate();

        entityManager.clear();
        entityManager.find(Article.class,id);
        return entityManager.getReference(Article.class,id);
    }

    //TODO: delete article
    @Transactional
    public Article deleteArticle(int id) {
        Query query = entityManager.createQuery("Delete from Article a WHERE a.id = :id")
                .setParameter("id",id);
        Article article = new Article();
        query.executeUpdate();
        return article;
    }


    //TODO: get article by category title
    @Transactional
    public List<Article> getArticleByCategoryTitle(String categoryTitle) {
        Query query = entityManager.createQuery("select a from Article a where a.category.title = :categoryTitle",Article.class)
            .setParameter("categoryTitle",categoryTitle);
        return query.getResultList();
    }
}
