package com.homework.viwatspringjpa.controller;

import com.homework.viwatspringjpa.message.BaseResponse;
import com.homework.viwatspringjpa.model.Article;
import com.homework.viwatspringjpa.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/restTemplate")
public class RestTemplateController {

    private RestTemplate restTemplate;

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    //TODO: Article RestTemplate
    //TODO: get all articles
    @GetMapping("/articles")
    public ResponseEntity<BaseResponse> getAllArticles(){
        ResponseEntity<BaseResponse> responseEntity =
                restTemplate.getForEntity("http://localhost:8080/articles",BaseResponse.class);
        return responseEntity;
    }

    //TODO: save article
    @PostMapping("/articles")
    public ResponseEntity<BaseResponse> saveArticle(@RequestBody Article articleRequest) {

        HttpEntity<Article> request = new HttpEntity<>(articleRequest);
        request.getBody().setCategory(articleRequest.getCategory());
        ResponseEntity<BaseResponse> responseEntity =
                restTemplate.postForEntity("http://localhost:8080/articles",request,BaseResponse.class);
        return responseEntity;
    }

    //TODO: get article by id
    @GetMapping("/articles/{id}")
    public ResponseEntity<BaseResponse> getOneArticle(@PathVariable int id){
        System.out.println(id);
        Map< String, Integer> params = new HashMap<>();
        params.put("id",id);
        ResponseEntity<BaseResponse> responseEntity =
                restTemplate.getForEntity("http://localhost:8080/articles/{id}",BaseResponse.class,params);
        return responseEntity;
    }

    //TODO: update article
    @PutMapping("/articles/{id}")
    public ResponseEntity<BaseResponse> updateArticle(@PathVariable int id,@RequestBody Article articleRequest) {
        final String uri = "http://localhost:8080/articles/{id}";
        HttpEntity<Article> request = new HttpEntity<>(articleRequest);
        request.getBody().setCategory(articleRequest.getCategory());
        Map<String, Integer> params = new HashMap<>();
        params.put("id", id);
        ResponseEntity<BaseResponse> responseEntity =
                restTemplate.exchange(uri,HttpMethod.PUT,request,BaseResponse.class,params);
        return responseEntity;
    }

    //TODO: delete article by id
    @DeleteMapping("/articles/{id}")
    public ResponseEntity<BaseResponse> deleteArticle(@PathVariable int id){
        final String uri = "http://localhost:8080/articles/{id}";
        Map<String, Integer> params = new HashMap<>();
        params.put("id", id);
        ResponseEntity<BaseResponse> responseEntity =
                restTemplate.exchange(uri,HttpMethod.DELETE,null,BaseResponse.class,params);
        return responseEntity;
    }

    //TODO: get article by id
    @GetMapping("/articles/search")
    public ResponseEntity<BaseResponse> getOneArticle(@RequestParam(value = "categoryTitle") String title){
        System.out.println(title);
        Map< String, String> params = new HashMap<>();
        params.put("categoryTitle",title);
        ResponseEntity<BaseResponse> responseEntity =
                restTemplate.getForEntity("http://localhost:8080/articles/search?categoryTitle={categoryTitle}",BaseResponse.class,params);
        return responseEntity;
    }


    //TODO: Category RestTemplate
    //TODO: get all category
    @GetMapping("/categories")
    public ResponseEntity<BaseResponse> getAllCategories(){

        ResponseEntity<BaseResponse> response =
                restTemplate.getForEntity("http://localhost:8080/categories",BaseResponse.class);
        return response;
    }

    //TODO: get category by id
    @GetMapping("/categories/{id}")
    public ResponseEntity<BaseResponse> getOneCategory(@PathVariable int id){
        System.out.println(id);
        Map< String, Integer> params = new HashMap<>();
        params.put("id",id);
        ResponseEntity<BaseResponse> responseEntity =
                restTemplate.getForEntity("http://localhost:8080/categories/{id}",BaseResponse.class,params);
        return responseEntity;
    }

    //TODO: save category
    @PostMapping("/categories")
    public ResponseEntity<BaseResponse> saveCategory(@RequestBody Category categoryRequest) {
        HttpEntity<Category> request = new HttpEntity<>(categoryRequest);
        ResponseEntity<BaseResponse> responseEntity =
                restTemplate.postForEntity("http://localhost:8080/categories",request,BaseResponse.class);
        return responseEntity;
    }

    //TODO: update category
    @PutMapping("/categories/{id}")
    public ResponseEntity<BaseResponse> updateCategory(@PathVariable int id,@RequestBody Category categoryRequest) {
        final String uri = "http://localhost:8080/categories/{id}";
        HttpEntity<Category> request = new HttpEntity<>(categoryRequest);
        Map<String, Integer> params = new HashMap<>();
        params.put("id", id);
        ResponseEntity<BaseResponse> responseEntity =
                restTemplate.exchange(uri,HttpMethod.PUT,request,BaseResponse.class,params);
        return responseEntity;
    }

    //TODO: delete category by id
    @DeleteMapping("/categories/{id}")
    public ResponseEntity<BaseResponse> deleteCategory(@PathVariable int id){
        final String uri = "http://localhost:8080/categories/{id}";
        Map<String, Integer> params = new HashMap<>();
        params.put("id", id);
        ResponseEntity<BaseResponse> responseEntity =
                restTemplate.exchange(uri,HttpMethod.DELETE,null,BaseResponse.class,params);
        return responseEntity;
    }

}
