package com.homework.viwatspringjpa.controller;

import com.homework.viwatspringjpa.message.BaseResponse;
import com.homework.viwatspringjpa.model.Article;
import com.homework.viwatspringjpa.service.ArticleServiceImpl;
import com.homework.viwatspringjpa.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    private ArticleServiceImpl articleService;

    @Autowired
    public void setArticleService(ArticleServiceImpl articleService) {
        this.articleService = articleService;
    }

    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    //TODO: get all articles
    @GetMapping()
    public ResponseEntity<BaseResponse<List<Article>>> getAllArticles(){
        BaseResponse<List<Article>> baseResponse = new BaseResponse<>();
        List<Article> articleList = articleService.getAllArticle();
        if(articleList.isEmpty()) {
            baseResponse.setMessage("Data has not found in database");
            baseResponse.setData(new ArrayList<>());
            baseResponse.setStatus(HttpStatus.NO_CONTENT);
        }
        else{
            baseResponse.setMessage("Data have been found successfully");
            baseResponse.setData(articleList);
            baseResponse.setStatus(HttpStatus.OK);
        }
        baseResponse.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(baseResponse);
    }

    //TODO: save article
    @PostMapping()
    public ResponseEntity<BaseResponse<Article>> saveArticle(@RequestBody Article articleRequest){
        BaseResponse<Article> articleBaseResponse = new BaseResponse<>();

        if(articleRequest.getAuthor().isEmpty()){
            articleBaseResponse.setMessage("Author cannot be empty");
            articleBaseResponse.setStatus(HttpStatus.BAD_REQUEST);
        }
        else if(articleRequest.getDescription().isEmpty()){
            articleBaseResponse.setMessage("Description cannot be empty");
            articleBaseResponse.setStatus(HttpStatus.BAD_REQUEST);
        }
        else if(articleRequest.getTitle().isEmpty()){
            articleBaseResponse.setMessage("Title cannot be empty");
            articleBaseResponse.setStatus(HttpStatus.BAD_REQUEST);
        }
        else if(articleRequest.getCategory().getId()==0){
            articleBaseResponse.setMessage("Category cannot be empty");
            articleBaseResponse.setStatus(HttpStatus.BAD_REQUEST);
        }
        else {
            Article article = articleService.saveArticle(articleRequest);

            articleBaseResponse.setMessage("Date has been inserted successfully");
            articleBaseResponse.setData(article);
            articleBaseResponse.setStatus(HttpStatus.CREATED);
        }

        articleBaseResponse.setTimestamp(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(articleBaseResponse);
    }

    //TODO: get one articles by id
    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<Article>> getOneArticle(@PathVariable int id){
        BaseResponse<Article> articleBaseResponse = new BaseResponse<>();
        Article article = articleService.getOneArticle(id);

        if (article == null) {
            articleBaseResponse.setMessage("Data has not found in database");
            articleBaseResponse.setData(null);
            articleBaseResponse.setStatus(HttpStatus.NO_CONTENT);
        } else {
            articleBaseResponse.setMessage("Data has been found successfully");
            articleBaseResponse.setData(article);
            articleBaseResponse.setStatus(HttpStatus.OK);
        }

        articleBaseResponse.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(articleBaseResponse);
    }

    //TODO: update article by id
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponse<Article>> updateArticle(@PathVariable int id, @RequestBody Article articleRequest){
        BaseResponse<Article> articleBaseResponse = new BaseResponse<>();

        if(articleRequest.getAuthor().isEmpty()){
            articleBaseResponse.setMessage("Author cannot be empty");
            articleBaseResponse.setStatus(HttpStatus.BAD_REQUEST);
        }
        else if(articleRequest.getDescription().isEmpty()){
            articleBaseResponse.setMessage("Description cannot be empty");
            articleBaseResponse.setStatus(HttpStatus.BAD_REQUEST);
        }
        else if(articleRequest.getTitle().isEmpty()){
            articleBaseResponse.setMessage("Title cannot be empty");
            articleBaseResponse.setStatus(HttpStatus.BAD_REQUEST);
        }
        else if(articleRequest.getCategory().getId()==0){
            articleBaseResponse.setMessage("Category cannot be empty");
            articleBaseResponse.setStatus(HttpStatus.BAD_REQUEST);
        }
        else{
            articleService.updateArticle(id, articleRequest);
            Article article = articleService.getOneArticle(id);

            if (article == null) {
                articleBaseResponse.setMessage("Failed to update, id has not found in database");
                articleBaseResponse.setData(null);
                articleBaseResponse.setStatus(HttpStatus.NO_CONTENT);
            } else {
                articleBaseResponse.setMessage("Data has been updated successfully");
                articleBaseResponse.setData(article);
                articleBaseResponse.setStatus(HttpStatus.OK);
            }
        }

        articleBaseResponse.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(articleBaseResponse);
    }

    //TODO: delete category by id
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<Article>> deleteArticle(@PathVariable int id){
        BaseResponse<Article> articleBaseResponse = new BaseResponse<>();
        Article article = articleService.getOneArticle(id);
        articleService.deleteArticle(id);

        if(article == null){
            articleBaseResponse.setMessage("Failed to delete, id has not found in database");
            articleBaseResponse.setData(null);
            articleBaseResponse.setStatus(HttpStatus.NO_CONTENT);
        }
        else {
            articleBaseResponse.setMessage("Data has been deleted successfully");
            articleBaseResponse.setData(article);
            articleBaseResponse.setStatus(HttpStatus.OK);
        }

        articleBaseResponse.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(articleBaseResponse);
    }

    //TODO: get article by category title
    @GetMapping("/search")
    public ResponseEntity<BaseResponse<List<Article>>> getArticleByCategoryTitle(@RequestParam("categoryTitle") String title){
        BaseResponse<List<Article>> baseResponse = new BaseResponse<>();
        List<Article> articleByCategoryTitle = articleService.getArticleByCategoryTitle(title);

        if(articleByCategoryTitle.isEmpty()){
            baseResponse.setMessage("Data has not found in database");
            baseResponse.setData(new ArrayList<>());
            baseResponse.setStatus(HttpStatus.NO_CONTENT);
        }
        else{
            baseResponse.setMessage("Data have been found successfully");
            baseResponse.setData(articleByCategoryTitle);
            baseResponse.setStatus(HttpStatus.OK);
        }
        baseResponse.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(baseResponse);
    }
}
