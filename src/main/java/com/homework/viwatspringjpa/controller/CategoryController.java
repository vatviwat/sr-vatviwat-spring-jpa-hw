package com.homework.viwatspringjpa.controller;

import com.homework.viwatspringjpa.message.BaseResponse;
import com.homework.viwatspringjpa.model.Category;
import com.homework.viwatspringjpa.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    //TODO: get all category
    @GetMapping()
    public ResponseEntity<BaseResponse<List<Category>>> getAllCategory(){
        BaseResponse<List<Category>> baseResponse = new BaseResponse<>();
        List<Category> categoryList = new ArrayList<>();
        categoryList = categoryService.getAllCategory();
        if(categoryList.isEmpty()){
            baseResponse.setMessage("Data has not found in database");
            baseResponse.setData(new ArrayList<>());
            baseResponse.setStatus(HttpStatus.NO_CONTENT);
        }
        else{
            baseResponse.setMessage("Data have been found successfully");
            baseResponse.setData(categoryList);
            baseResponse.setStatus(HttpStatus.OK);
        }
        baseResponse.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(baseResponse);
    }

    //TODO:  save category
    @PostMapping()
    public ResponseEntity<BaseResponse<Category>> saveCategory(@RequestBody Category categoryRequest){
        BaseResponse<Category> categoryBaseResponse = new BaseResponse<>();
        if(categoryRequest.getTitle().isEmpty()){
            categoryBaseResponse.setMessage("Title cannot be empty");
            categoryBaseResponse.setStatus(HttpStatus.BAD_REQUEST);
        }
        else{
            Category category = categoryService.saveCategory(categoryRequest);

            categoryBaseResponse.setMessage("Data have been added successfully");
            categoryBaseResponse.setData(category);
            categoryBaseResponse.setStatus(HttpStatus.CREATED);
        }

        categoryBaseResponse.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(categoryBaseResponse);
    }

    //TODO: get category by id
    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<Category>> getOneCategory(@PathVariable int id){
        BaseResponse<Category> categoryBaseResponse = new BaseResponse<>();
        Category category = categoryService.getOneCategory(id);

        if(category == null){
            categoryBaseResponse.setMessage("Data has not found in database");
            categoryBaseResponse.setData(category);
            categoryBaseResponse.setStatus(HttpStatus.NO_CONTENT);
        }
        else {

            categoryBaseResponse.setMessage("Data has been found successfully");
            categoryBaseResponse.setData(category);
            categoryBaseResponse.setStatus(HttpStatus.OK);
        }

        categoryBaseResponse.setTimestamp(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(categoryBaseResponse);
    }

    //TODO: update category by id
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponse<Category>> getOneCategory(@PathVariable int id,@RequestBody  Category categoryRequest){
        BaseResponse<Category> categoryBaseResponse = new BaseResponse<>();
        categoryService.updateCategory(id,categoryRequest);
        Category category = categoryService.getOneCategory(id);

        if(category == null){
            categoryBaseResponse.setMessage("Failed to update, id has not found in database");
            categoryBaseResponse.setData(null);
            categoryBaseResponse.setStatus(HttpStatus.NO_CONTENT);
        }
        else {
            categoryBaseResponse.setMessage("Data has been updated successfully");
            categoryBaseResponse.setData(category);
            categoryBaseResponse.setStatus(HttpStatus.OK);
        }

        categoryBaseResponse.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(categoryBaseResponse);
    }

    //TODO: delete category by id
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<Category>> deleteCategory(@PathVariable int id){
        BaseResponse<Category> categoryBaseResponse = new BaseResponse<>();

        Category category = categoryService.getOneCategory(id);
        categoryService.deleteCategory(id);

        if(category == null){
            categoryBaseResponse.setMessage("Failed to delete, id has not found in database");
            categoryBaseResponse.setData(null);
            categoryBaseResponse.setStatus(HttpStatus.NO_CONTENT);
        }
        else {
            categoryBaseResponse.setMessage("Data has been deleted successfully");
            categoryBaseResponse.setData(category);
            categoryBaseResponse.setStatus(HttpStatus.OK);
        }

        categoryBaseResponse.setTimestamp(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(categoryBaseResponse);
    }
}
